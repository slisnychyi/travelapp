# Travel Guide Service
 
### frameworks/libraries in use:
- *Lombok* - for removing boilerplate code
- *Spring mvc* - with async request processing  
- *Spring data* - for comunication with database
- *embadded mongodb* - for storing bookings data 

##### how to run:
1. Run the application by using `./mvnw spring-boot:run`
2. Alternatively, build the JAR file with `./mvnw clean package` 
and then run the JAR file, as follows: `java -jar target/travelservice-0.0.1-SNAPSHOT.jar`

##### endpoints:

| Endpoint                                  | Method  | URL                                 |
|-------------------------------------------|---------|-------------------------------------|
| Get Booking by uuid                       | `GET`   | `/api/v1/booking/{uuid}`            |
| Get Bookings by date (yyyy-MM-dd format)  | `GET`   | `/api/v1/booking?date={date}`       |
| Delete Booking by uuid                    | `DELETE`| `/api/v1/booking/{uuid}`            |
| Save new Booking                          | `POST`  | `/api/v1/booking`                   |

*example of booking save body request*:
```json 
{
"name":"SHIRE",
"hikers": [
	{
	    "name":"Jack",
		"age":20
	}
]
}
```
  
  
##### items to mention
- not implemented security access.
System requests such as get bookings for specific date should by secured by user roles or another security approaches.
- used embedded database that probabbly can be used for integration tests and shouldn't be in use in production.
 