package com.slisnychyi.travelservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slisnychyi.travelservice.model.Booking;
import com.slisnychyi.travelservice.model.Hiker;
import com.slisnychyi.travelservice.model.TrailName;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.model.response.GenericResponseFactory;
import com.slisnychyi.travelservice.service.BookingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = BookingController.class)
public class BookingControllerTest {

    @Autowired
    MockMvc mvc;
    @MockBean
    BookingService bookingService;
    @MockBean
    GenericResponseFactory responseFactory;
    @Autowired
    ObjectMapper mapper;

    @Test
    public void should_getBookingsData_when_valid_bookingDateRequest() throws Exception {
        //given
        LocalDate date = LocalDate.parse("2020-01-10", DateTimeFormatter.ofPattern(BookingService.BOOKING_DATE_FORMAT));
        Booking hiker = Booking.of(TrailName.GONDOR, List.of(new Hiker().setName("hiker").setAge(20)));
        when(bookingService.getBookingsByDate(date))
                .thenReturn(List.of(hiker));

        //when
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/v1/booking")
                .contentType(MediaType.APPLICATION_JSON)
                .param("date", "2020-01-10"))
                .andExpect(status().isOk())
                .andReturn();

        List<Booking> bookings = (List<Booking>) result.getAsyncResult();
        //then
        assertThat(bookings.size()).isEqualTo(1);
        assertThat(bookings).contains(hiker);
    }

    @Test
    public void should_saveBookingRequest_when_valid_bookingRequest() throws Exception {
        //given
        BookingRequest bookingRequest = new BookingRequest()
                .setName(TrailName.GONDOR)
                .setHikers(List.of(new Hiker().setName("hiker").setAge(15)));
        when(bookingService.saveBooking(bookingRequest))
                .thenReturn("bookingUuid");
        //when
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/v1/booking")
                .content(mapper.writeValueAsString(bookingRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        Map<String, String> asyncResult = (Map<String, String>) result.getAsyncResult();
        //then
        assertThat(asyncResult.get("uuid")).isEqualTo("bookingUuid");
    }

    @Test
    public void should_not_saveBookingRequest_when_invalid_bookingRequest() throws Exception {
        //given
        BookingRequest bookingRequest = new BookingRequest()
                .setName(TrailName.GONDOR);
        //when
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/booking")
                .content(mapper.writeValueAsString(bookingRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void should_not_getBookingsData_when_invalid_bookingDateRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/booking")
                .contentType(MediaType.APPLICATION_JSON)
                .param("date", "2020-JAN-10"))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

}