package com.slisnychyi.travelservice.service;

import com.slisnychyi.travelservice.errorhandler.EntityNotFoundException;
import com.slisnychyi.travelservice.model.Booking;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.repository.BookingRepository;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

class BookingServiceTest {

    @Test
    public void should_getBooking_by_validUuid() {
        //given
        BookingRepository bookingRepository = mock(BookingRepository.class);
        BookingService service = new BookingService(bookingRepository, null);
        BookingRequest bookingRequest = new BookingRequest();
        Booking booking = Booking.of(bookingRequest.getName(), bookingRequest.getHikers());
        when(bookingRepository.getByUuid("uuid")).thenReturn(Optional.of(booking));
        //when
        Booking result = service.getBooking("uuid");
        //then
        assertThat(result.getUuid()).isNotEmpty();
    }

    @Test
    public void should_getBooking_by_validDate() {
        //given
        BookingRepository bookingRepository = mock(BookingRepository.class);
        BookingService service = new BookingService(bookingRepository, null);
        LocalDate date = LocalDate.now();
        BookingRequest bookingRequest = new BookingRequest();
        Booking booking = Booking.of(bookingRequest.getName(), bookingRequest.getHikers());
        LocalDateTime from = date.atStartOfDay();
        LocalDateTime to = date.plusDays(1).atStartOfDay();
        when(bookingRepository.findByDate(from, to)).thenReturn(Optional.of(List.of(booking)));
        //when
        List<Booking> result = service.getBookingsByDate(date);
        //then
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void should_throwException_when_noBooking_by_uuid() {
        //given
        BookingRepository bookingRepository = mock(BookingRepository.class);
        BookingService service = new BookingService(bookingRepository, null);
        when(bookingRepository.getByUuid("uuid")).thenReturn(Optional.empty());
        //then
        assertThrows(EntityNotFoundException.class, () -> service.getBooking("uuid"));
    }

    @Test
    public void should_throwException_when_noBooking_by_date() {
        //given
        BookingRepository bookingRepository = mock(BookingRepository.class);
        BookingService service = new BookingService(bookingRepository, null);
        LocalDate date = LocalDate.now();
        LocalDateTime from = date.atStartOfDay();
        LocalDateTime to = date.plusDays(1).atStartOfDay();
        when(bookingRepository.findByDate(from, to)).thenReturn(Optional.empty());
        //then
        assertThrows(EntityNotFoundException.class, () -> service.getBookingsByDate(date));
    }

}