package com.slisnychyi.travelservice.service.validator;

import com.slisnychyi.travelservice.errorhandler.InvalidDataException;
import com.slisnychyi.travelservice.model.Hiker;
import com.slisnychyi.travelservice.model.Trail;
import com.slisnychyi.travelservice.model.TrailName;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.repository.TrailsRepository;
import org.junit.jupiter.api.Test;

import java.time.temporal.ValueRange;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class BookingValidatorTest {

    @Test
    public void should_throwException_when_noSuchTrail() {
        //given
        TrailsRepository trailsRepository = mock(TrailsRepository.class);
        BookingValidator validator = new BookingValidator(trailsRepository);
        BookingRequest bookingRequest = new BookingRequest().setName(TrailName.GONDOR);
        //then
        assertThrows(InvalidDataException.class, () -> validator.isValidBookingRequest(bookingRequest));
    }

    @Test
    public void should_returnFalse_when_invalidHikersAge() {
        //given
        TrailsRepository trailsRepository = mock(TrailsRepository.class);
        BookingValidator validator = new BookingValidator(trailsRepository);
        when(trailsRepository.getByName(TrailName.GONDOR)).thenReturn(Optional.of(new Trail().setAgeLimit(ValueRange.of(10, 20))));
        BookingRequest bookingRequest = new BookingRequest()
                .setName(TrailName.GONDOR)
                .setHikers(List.of(
                        new Hiker().setAge(10),
                        new Hiker().setAge(9),
                        new Hiker().setAge(19)
                ));
        //when
        boolean result = validator.isValidBookingRequest(bookingRequest);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void should_returnTrue_when_validHikersAge() {
        //given
        TrailsRepository trailsRepository = mock(TrailsRepository.class);
        BookingValidator validator = new BookingValidator(trailsRepository);
        when(trailsRepository.getByName(TrailName.GONDOR)).thenReturn(Optional.of(new Trail().setAgeLimit(ValueRange.of(10, 20))));
        BookingRequest bookingRequest = new BookingRequest()
                .setName(TrailName.GONDOR)
                .setHikers(List.of(
                        new Hiker().setAge(10),
                        new Hiker().setAge(19),
                        new Hiker().setAge(15),
                        new Hiker().setAge(20)
                ));
        //when
        boolean result = validator.isValidBookingRequest(bookingRequest);
        //then
        assertThat(result).isTrue();
    }

}