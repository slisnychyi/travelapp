package com.slisnychyi.travelservice.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.slisnychyi.travelservice.model.Trail;
import com.slisnychyi.travelservice.model.TrailName;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Optional.*;
import static java.util.stream.Collectors.*;

@Component
public class StubbedTrailRepository implements TrailsRepository {

    private static final String TRAILS_CONFIGURATION_FILE = "/trails.json";
    private static final TypeReference<List<Trail>> TRAILS_TYPE_REFERENCE = new TypeReference<>() {
    };

    private final ObjectMapper mapper;
    private Map<TrailName, Trail> trails;

    public StubbedTrailRepository(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @PostConstruct
    public void init() throws IOException {
        List<Trail> trails = mapper.readValue(getClass().getResourceAsStream(TRAILS_CONFIGURATION_FILE), TRAILS_TYPE_REFERENCE);
        this.trails = trails.stream()
                .collect(toMap(Trail::getName, Function.identity()));
    }

    @Override
    public Optional<Trail> getByName(TrailName name) {
        return ofNullable(trails.get(name));
    }

}
