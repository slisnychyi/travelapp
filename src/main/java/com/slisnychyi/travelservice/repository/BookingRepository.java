package com.slisnychyi.travelservice.repository;

import com.slisnychyi.travelservice.model.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BookingRepository extends MongoRepository<Booking, String> {

    Optional<Booking> getByUuid(String uuid);

    @Query("{ 'date':{'$gte':{'$date':?0}, '$lt':{'$date':?1} } }")
    Optional<List<Booking>> findByDate(LocalDateTime from, LocalDateTime to);

}