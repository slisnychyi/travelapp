package com.slisnychyi.travelservice.repository;

import com.slisnychyi.travelservice.model.Trail;
import com.slisnychyi.travelservice.model.TrailName;
import java.util.Optional;

public interface TrailsRepository {

  Optional<Trail> getByName(TrailName name);

}
