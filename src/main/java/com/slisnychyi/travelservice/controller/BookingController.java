package com.slisnychyi.travelservice.controller;

import com.slisnychyi.travelservice.controller.validator.ValidBookingRequest;
import com.slisnychyi.travelservice.model.Booking;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.model.response.GenericResponse;
import com.slisnychyi.travelservice.model.response.GenericResponseFactory;
import com.slisnychyi.travelservice.service.BookingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import static java.util.Collections.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/booking",
        produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class BookingController {

    private final BookingService bookingService;
    private final GenericResponseFactory genericResponse;

    @GetMapping
    public Callable<List<Booking>> getBookings(@RequestParam
                                               @DateTimeFormat(pattern = BookingService.BOOKING_DATE_FORMAT) LocalDate date) {
        log.info("Received system request to get bookings by date={}", date);
        return () -> bookingService.getBookingsByDate(date);
    }

    @PostMapping
    public Callable<Map<String, String>> saveBooking(@RequestBody @ValidBookingRequest BookingRequest bookingRequest) {
        log.info("Received request to save booking [trailName={}, hikersSize={}]",
                bookingRequest.getName(), bookingRequest.getHikers().size());
        return () -> singletonMap("uuid", bookingService.saveBooking(bookingRequest));
    }

    @GetMapping("/{uuid}")
    public Callable<Booking> getBooking(@PathVariable String uuid) {
        log.info("Received request to get booking by uuid={}", uuid);
        return () -> bookingService.getBooking(uuid);
    }

    @DeleteMapping("/{uuid}")
    public Callable<GenericResponse> deleteBooking(@PathVariable String uuid) {
        log.info("Received request to delete booking by uuid={}", uuid);
        return () -> {
            bookingService.deleteBooking(uuid);
            log.info("Booking with uuid={} was deleted.", uuid);
            return genericResponse.createSuccessResponse();
        };
    }

}
