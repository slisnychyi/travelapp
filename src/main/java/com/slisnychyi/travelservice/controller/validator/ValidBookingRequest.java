package com.slisnychyi.travelservice.controller.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;

@Target({PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BookingRequestValidator.class)
public @interface ValidBookingRequest {
  String message() default "invalid booking request";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
