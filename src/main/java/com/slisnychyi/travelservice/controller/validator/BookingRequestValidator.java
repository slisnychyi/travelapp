package com.slisnychyi.travelservice.controller.validator;

import com.slisnychyi.travelservice.model.request.BookingRequest;
import org.apache.commons.collections4.CollectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.nonNull;
import static java.util.Optional.of;

public class BookingRequestValidator implements ConstraintValidator<ValidBookingRequest, BookingRequest> {

  @Override
  public boolean isValid(BookingRequest bookingRequest, ConstraintValidatorContext constraintValidatorContext) {
    return of(bookingRequest)
      .filter(e -> nonNull(e.getName()))
      .map(BookingRequest::getHikers)
      .filter(CollectionUtils::isNotEmpty)
      .map(hikers -> hikers.stream().allMatch(hiker -> hiker.getAge() > 0 && nonNull(hiker.getName())))
      .orElse(false);
  }
}
