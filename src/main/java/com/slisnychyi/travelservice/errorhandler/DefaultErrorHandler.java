package com.slisnychyi.travelservice.errorhandler;

import com.slisnychyi.travelservice.model.response.GenericResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;

@ControllerAdvice
public class DefaultErrorHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<GenericResponse> handleValidationException(ConstraintViolationException exception) {
        String message = ofNullable(exception.getConstraintViolations())
                .filter(CollectionUtils::isNotEmpty)
                .map(violations -> violations.stream()
                        .map(ConstraintViolation::getMessage)
                        .collect(joining(",")))
                .orElse("constraint violation");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new GenericResponse(message));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<GenericResponse> handleNotFoundException(EntityNotFoundException exception) {
        String message = ofNullable(exception.getMessage())
                .orElse("entity not found");
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new GenericResponse(message));
    }


}
