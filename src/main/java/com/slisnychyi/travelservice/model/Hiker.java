package com.slisnychyi.travelservice.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Hiker {
  private String name;
  private int age;
}
