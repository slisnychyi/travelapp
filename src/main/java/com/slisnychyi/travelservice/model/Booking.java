package com.slisnychyi.travelservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.List;

import static com.slisnychyi.travelservice.service.BookingService.BOOKING_DATE_FORMAT;
import static java.time.LocalDateTime.*;
import static java.util.UUID.*;

@Getter
public class Booking {

    @Id
    private String uuid;
    @JsonFormat(pattern = BOOKING_DATE_FORMAT)
    private LocalDateTime date;
    private TrailName trailName;
    private List<Hiker> hikers;

    private Booking(TrailName trailName, List<Hiker> hikers) {
        this.uuid = randomUUID().toString();
        this.date = now();
        this.trailName = trailName;
        this.hikers = hikers;
    }

    public static Booking of(TrailName trailName, List<Hiker> hikers) {
        return new Booking(trailName, hikers);
    }

}
