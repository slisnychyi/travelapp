package com.slisnychyi.travelservice.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.temporal.ValueRange;

import static java.lang.Integer.valueOf;

public class AgeLimitDeserializer extends JsonDeserializer<ValueRange> {
  @Override
  public ValueRange deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    String[] ageLimit = jsonParser.readValueAs(String.class).split("-");
    return ValueRange.of(valueOf(ageLimit[0].trim()), valueOf(ageLimit[1].trim()));
  }
}
