package com.slisnychyi.travelservice.model.request;

import com.slisnychyi.travelservice.model.Hiker;
import com.slisnychyi.travelservice.model.TrailName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class BookingRequest {

  private TrailName name;
  private List<Hiker> hikers;

}
