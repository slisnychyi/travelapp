package com.slisnychyi.travelservice.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Optional;

public enum TrailName {

  SHIRE, GONDOR, MORDOR;

  @JsonCreator
  public static TrailName of(String name) {
    return Optional.ofNullable(name)
      .map(String::toUpperCase)
      .map(TrailName::valueOf)
      .orElse(null);
  }

}
