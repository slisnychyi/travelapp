package com.slisnychyi.travelservice.model.response;

import org.springframework.stereotype.Component;

@Component
public class GenericResponseFactory {

    public GenericResponse createSuccessResponse() {
        return new GenericResponse("success");
    }

}
