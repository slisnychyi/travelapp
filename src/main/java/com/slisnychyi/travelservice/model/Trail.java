package com.slisnychyi.travelservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.temporal.ValueRange;

@Data
@Accessors(chain = true)
public class Trail {

    private TrailName name;
    @JsonProperty("start")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private LocalTime startTime;
    @JsonProperty("end")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private LocalTime endTime;
    @JsonDeserialize(using = AgeLimitDeserializer.class)
    private ValueRange ageLimit;
    private BigDecimal price;


}
