package com.slisnychyi.travelservice.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class WebMvcConfiguration {

  private static final int DEFAULT_POOL_SIZE = 2;
  private static final int DEFAULT_QUEUE_SIZE = 20;

  @Bean
  public AsyncTaskExecutor asyncTaskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(DEFAULT_POOL_SIZE);
    executor.setQueueCapacity(DEFAULT_QUEUE_SIZE);
    executor.setRejectedExecutionHandler((r, e) -> {
      long notCompleted = e.getTaskCount() - e.getCompletedTaskCount();
      log.error("Reject request due to low capacity in thread pool queue running [{}] vs poolSize [{}]",
        notCompleted, DEFAULT_QUEUE_SIZE);
    });
    executor.setWaitForTasksToCompleteOnShutdown(true);
    return executor;
  }

  @Bean
  public WebMvcConfigurer webMvcConfigurer(AsyncTaskExecutor asyncTaskExecutor) {
    return new WebMvcConfigurer() {
      @Override
      public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        configurer.setTaskExecutor(asyncTaskExecutor);
      }
    };
  }

}
