package com.slisnychyi.travelservice.service.validator;

import com.slisnychyi.travelservice.errorhandler.InvalidDataException;
import com.slisnychyi.travelservice.model.TrailName;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.repository.TrailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingValidator {

    private final TrailsRepository trailsRepository;

    public boolean isValidBookingRequest(BookingRequest bookingRequest) {
        TrailName trailName = bookingRequest.getName();
        return trailsRepository.getByName(trailName)
                .map(trail -> bookingRequest.getHikers().stream()
                        .allMatch(hiker -> trail.getAgeLimit().isValidIntValue(hiker.getAge())))
                .orElseThrow(() -> {
                    String message = "Can't find trail by trainName=" + trailName;
                    log.error(message);
                    return new InvalidDataException(message);
                });
    }

}
