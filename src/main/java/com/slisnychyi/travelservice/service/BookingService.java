package com.slisnychyi.travelservice.service;

import com.slisnychyi.travelservice.errorhandler.EntityNotFoundException;
import com.slisnychyi.travelservice.errorhandler.InvalidDataException;
import com.slisnychyi.travelservice.model.Booking;
import com.slisnychyi.travelservice.model.request.BookingRequest;
import com.slisnychyi.travelservice.repository.BookingRepository;
import com.slisnychyi.travelservice.service.validator.BookingValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Optional.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingService {

    public static final String BOOKING_DATE_FORMAT = "yyyy-MM-dd";

    private final BookingRepository bookingRepository;
    private final BookingValidator bookingValidator;

    public String saveBooking(BookingRequest bookingRequest) {
        return of(bookingRequest)
                .filter(bookingValidator::isValidBookingRequest)
                .map(request -> Booking.of(request.getName(), request.getHikers()))
                .map(bookingRepository::save)
                .map(Booking::getUuid)
                .orElseThrow(() -> {
                    String message = "Can't save booking request.";
                    log.error(message);
                    return new InvalidDataException(message);
                });
    }

    public void deleteBooking(String uuid) {
        Booking booking = getBooking(uuid);
        bookingRepository.delete(booking);
    }

    public Booking getBooking(String uuid) {
        return bookingRepository.getByUuid(uuid)
                .orElseThrow(() -> {
                    String message = String.format("booking with uuid=%s not found", uuid);
                    log.error(message);
                    return new EntityNotFoundException(message);
                });
    }

    public List<Booking> getBookingsByDate(LocalDate date) {
        LocalDateTime from = date.atStartOfDay();
        LocalDateTime to = date.plusDays(1).atStartOfDay();
        return bookingRepository.findByDate(from, to)
                .orElseThrow(() -> {
                    String message = String.format("booking with time=%s not found", date);
                    log.error(message);
                    return new EntityNotFoundException(message);
                });
    }

}
